import requests
from requests.auth import HTTPBasicAuth

from django.conf import settings


class AuthClient:

    def __init__(self, base_url):
        self.base_url = base_url
        self.auth = HTTPBasicAuth(settings.URB_AUTH_USER, settings.URB_AUTH_PASS)

    def get_public_key(self):
        response = requests.get(self.base_url + 'urb/auth/users/public_key/')
        return response.json()['public_key']

    def register(self, email: str, password: str, first_name: str, last_name: str):
        body = {
            'username': email,
            'password': password,
            'first_name': first_name,
            'last_name': last_name,
        }
        requests.post(self.base_url + 'urb/auth/users/register_encrypt/', json=body, auth=self.auth)
