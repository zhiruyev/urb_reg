import base64

from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA

ENCRYPTION_KEY_BITS = 2048
SENTINEL_BITS = 16
ENCODING_TYPE = "utf-8"


from libs.exceptions import EncryptionErrorException


class RSAEncryption:
    def __init__(self):
        self.bits = ENCRYPTION_KEY_BITS

    def _helper(self, key: bytes) -> str:
        return "\\n".join(key.decode(ENCODING_TYPE).splitlines())

    @staticmethod
    def _cipher(key: bytes):
        key = RSA.import_key(key)
        return PKCS1_v1_5.new(key)

    def encrypt(self, public_key: str, message: str) -> str:
        public_key = public_key.encode(ENCODING_TYPE)
        encryptor = self._cipher(public_key)
        try:
            encrypted = base64.b64encode(
                encryptor.encrypt(message.encode(ENCODING_TYPE))
            )
            encrypted = encrypted.decode(ENCODING_TYPE)
        except Exception as e:
            raise EncryptionErrorException(detail=str(e))
        else:
            return encrypted
