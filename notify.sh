TIME="10"
URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"
TEXT="#$CI_PROJECT_NAME #$CI_PIPELINE_ID
$CI_COMMIT_MESSAGE $GITLAB_USER_LOGIN"

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_CHAT_ID&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null