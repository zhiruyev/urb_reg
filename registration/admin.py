from django.contrib import admin
from registration.models import (
    RegistrationApplication,
    RegistrationApplicationCode,
)


@admin.register(RegistrationApplication)
class RegistrationApplicationAdmin(admin.ModelAdmin):
    list_display = (
        'email',
        'status',
    )

    list_filter = (
        'status',
    )


@admin.register(RegistrationApplicationCode)
class RegistrationApplicationCodeAdmin(admin.ModelAdmin):
    list_display = (
        'code',
    )
