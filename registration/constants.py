from libs.base_enum import BaseEnum


class RegistrationStatuses(BaseEnum):
    CREATED = 0
    EMAIL_CONFIRMED = 1
    FINISHED = 2
