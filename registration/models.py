import uuid

from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.utils.translation import gettext as _

from registration.constants import RegistrationStatuses


class RegistrationApplication(TimeStampedModel):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)

    email = models.CharField(max_length=200)
    status = models.PositiveSmallIntegerField(
        choices=RegistrationStatuses.choices(),
        default=RegistrationStatuses.CREATED.value
    )

    def email_confirmed(self):
        self.status = RegistrationStatuses.EMAIL_CONFIRMED.value
        self.save(update_fields=['status'])

    def finish(self):
        self.status = RegistrationStatuses.FINISHED.value
        self.save(update_fields=['status'])

    class Meta:
        verbose_name = _("Заявка на регистрацию")
        verbose_name_plural = _("Заявки на регистрацию")

    def __str__(self):
        return self.email


class RegistrationApplicationCode(TimeStampedModel):
    application = models.ForeignKey(
        RegistrationApplication,
        on_delete=models.SET_NULL,
        null=True
    )
    code = models.IntegerField()

    class Meta:
        verbose_name = _("Код подтверждения")
        verbose_name_plural = _("Коды подтверждений")

    def __str__(self):
        return str(self.code)
