from rest_framework import serializers

from libs.serializers import BaseSerializer
from registration.entities.registration_entities import (
    RegistrationInitInputEntity,
    RegistrationInitOutputEntity,
    RegistrationConfirmEmailInputEntity,
    RegistrationFinishInputEntity,
)


class RegistrationInitInputSerializer(BaseSerializer):
    first_name = serializers.CharField(max_length=200)
    last_name = serializers.CharField(max_length=200)
    email = serializers.CharField(max_length=200)

    def create(self, validated_data) -> RegistrationInitInputEntity:
        return RegistrationInitInputEntity(**validated_data)


class RegistrationInitOutputSerializer(BaseSerializer):
    uuid = serializers.UUIDField()
    status = serializers.IntegerField()

    def create(self, validated_data) -> RegistrationInitOutputEntity:
        return RegistrationInitOutputEntity(**validated_data)


class RegistrationConfirmEmailInputSerializer(BaseSerializer):
    code = serializers.IntegerField()

    def create(self, validated_data) -> RegistrationConfirmEmailInputEntity:
        return RegistrationConfirmEmailInputEntity(**validated_data)


class RegistrationFinishInputSerializer(BaseSerializer):
    password = serializers.CharField(max_length=1000)

    def create(self, validated_data) -> RegistrationFinishInputEntity:
        return RegistrationFinishInputEntity(**validated_data)
