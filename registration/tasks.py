from django.conf import settings
from django.core.mail import send_mail

from config.celery import app as celery_app


@celery_app.task(queue="mainq")
def send_confimation_code(confirmation_code: int, email: str):
    send_mail(
        'Код подтверждения регистрации Урбатон',
        f'Ваш код подтверждения: {confirmation_code}',
        settings.EMAIL_HOST_USER,
        [email],
        fail_silently=True
    )
