from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from registration.handlers.registration_handlers import RegistrationApplicationHandler
from registration.models import RegistrationApplication
from registration.serializers.registration_serializers import (
    RegistrationInitInputSerializer,
    RegistrationInitOutputSerializer,
    RegistrationConfirmEmailInputSerializer,
    RegistrationFinishInputSerializer,
)


class RegistrationApplicationViewSet(
    GenericViewSet,
):
    queryset = RegistrationApplication.objects.all()

    @swagger_auto_schema(
        operation_summary="Начало регистрации пользователя",
        request_body=RegistrationInitInputSerializer,
        responses={"200": RegistrationInitOutputSerializer}
    )
    @action(methods=['post'], detail=False)
    def init(self, request, *args, **kwargs):
        serializer = RegistrationInitInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()
        entity = RegistrationApplicationHandler().init(input_entity=input_entity)

        return Response(RegistrationInitOutputSerializer(entity).data)

    @swagger_auto_schema(
        operation_summary="Подтверждение email пользователя",
        request_body=RegistrationConfirmEmailInputSerializer,
        responses={"200": ""},
    )
    @action(methods=['post'], detail=True)
    def confirm_email(self, request, *args, **kwargs):
        serializer = RegistrationConfirmEmailInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        app = self.get_object()

        RegistrationApplicationHandler().confirm_email(
            application=app,
            input_entity=input_entity,
        )

        return Response()

    @swagger_auto_schema(
        operation_summary="Завершение регистрации пользователя",
        request_body=RegistrationFinishInputSerializer,
        responses={"200": ""},
    )
    @action(methods=['post'], detail=True)
    def finish(self, request, *args, **kwargs):
        serializer = RegistrationFinishInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        app = self.get_object()

        RegistrationApplicationHandler().finish(
            application=app,
            input_entity=input_entity,
        )

        return Response()
